# Тестовое задание.


##Задание №1.
```
SELECT hotel_id, COUNT(hotel_id) AS "count" FROM hotelrooms GROUP BY hotel_id HAVING COUNT(hotel_id) < 20;
```

##Задание №2.
```
from django.db import models

class HotelRooms(models.Model):
    
    room_id = models.PositiveIntegerField('номер комнаты', help_text='номер комнаты')
    hotel_id = models.PositiveIntegerField('отель', help_text='отель')
    price = models.PositiveIntegerField('цена',help_text='цена')

    def __str__(self) -> str:
        return str(self.room_id)
```
```
models.HotelsRooms.objects.values('hotel_id').annotate(count=Count('hotel_id')).filter(count__lt = 20)
```

##Задание №3.
```
    Для поднятия проекта необходим docker и docker-compose. 
```
```
    Для получения данных с сайта необходимо запустить команду  docker-compose up.
```
```
    Полученные результаты сохраняются в файле output_data/output_data.json
``` 

##Задание №4.
```
    Исплользование Scrapy.
```