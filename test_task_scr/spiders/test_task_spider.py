from scrapy.spiders import CrawlSpider, Rule
from test_task_scr.items import TestTaskScrItem
from urllib.parse import urljoin


class PycoderSpider(CrawlSpider):
    name = "test_task_scrapy"
    start_urls = [
        'https://www.lueftner-cruises.com/en/river-cruises/cruise.html',
    ]

    def parse(self, response):

        for post_link in response.xpath('//h3[@class="travel-box-heading"]/span/a/@href').extract():
            url = urljoin(response.url, post_link)
            yield response.follow(url, callback=self.parse_post)

    def parse_post(self, response):

        item = TestTaskScrItem()
        name = response.xpath(
            '//div[@class="cruise-headline"]/h1/text()').get()
        days = response.xpath('//p[@class="cruise-duration pull-right"]/text()').get()
        dates = response.xpath('//a[@class="btn btn-default btn-sm active"]/text()').get()
        itinerary_crude = response.css('div.route-station')
        list_itinerary = []
        for el in itinerary_crude:
            day = str(el.css('span.route-day::text').get())
            city = ' '.join(el.css('span.route-city::text').get().split())
            list_itinerary.append({day: city})
        item['name'] = name
        item['days'] = days
        item['itinerary'] = list_itinerary
        item['dates'] = dates
        yield item
